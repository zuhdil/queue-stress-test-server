<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use Psr\Log\LoggerInterface;

class SendEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $mail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message, $to)
    {
        $this->mail = [$message, $to];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer, LoggerInterface $logger)
    {
        list($message, $to) = $this->mail;

        $mailer->raw(e($message), function ($m) use ($message, $to) {
            $m->from('internal@example.com', $message);
            $m->to($to);
        });

        $logger->info(sprintf('Send email to %s with content "%s"', $to, $message));
    }
}
