<?php

namespace App\Providers;

use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\QueueManager;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(QueueManager $queue, LoggerInterface $logger)
    {
        $queue->failing(function (JobFailed $event) use ($logger) {
            // $event->connectionName
            // $event->job
            // $event->data

            $logger->error(var_export($event));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
