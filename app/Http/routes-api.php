<?php

use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;

/* @var $router Illuminate\Routing\Router */

$router->get('/push', function (Request $request, LoggerInterface $logger) {

    dispatch(new SendEmail($request->query('m', 'No message'), 'nobody@example.com'));

    $logger->debug('SendEmail dispatched');

    return response('OK');
});
